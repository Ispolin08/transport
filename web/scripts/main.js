/*
 * Hints Logic
 */
var hints = (function() {
	var
		hintBox,
		ballonsWrap;

	var init = function() {
		hintBox     = $('.hint');
		ballonsWrap = $('<div />', { class: 'hint-ballon-wrap' }).appendTo('body');

		hintBox.length && _events();
	};

	var _events = function() {
		hintBox.on( 'click', '.hint__icon', _hintAction );
		ballonsWrap.on('click', '.hint-ballon', _closeHint );

		// hintText = hintText.html();
	};

	var _hintAction = function( e ) {
		var
			$this    = $( this ),
			hintId   = $this.closest('.hint').data('hint-id'),
			content  = $this.closest('.hint').find('.hint__text').html(),
			position = $this.offset();

		_removeBox();

		e.preventDefault();

		if( hintId ) {
			if( $this.hasClass('hint__icon_stat-active') ) {
				_removeBox( hintId );
			} else {
				$this.addClass('hint__icon_stat-active');

				_createBox( position, hintId, content );
			}
		}
	};

	var _createBox = function( pos, hintId, hintText ) {
		var
			content = hintText || '',
			pos     = pos || false,
			hintId  = hintId || 0,
			element = {};

		if( pos ) {
			element = $('<div/>', {
				class: 'hint-ballon',
				text: content,
				css : {
					top: pos.top,
					left: pos.left
				}
			})
			.attr('data-hint-id', hintId)
			.appendTo( ballonsWrap );

			element.css({
				marginTop: -element.outerHeight() - 24,
				marginLeft: 10 - element.outerWidth()
			});
		}
		
	};

	var _removeBox = function( boxId ) {
		var
			boxId = boxId || 0;

		if( boxId ) {
			ballonsWrap
					.find('.hint-ballon[data-hint-id=' + boxId + ']')
					.remove();

			hintBox
				.filter('[data-hint-id=' + boxId + ']')
				.find('> .hint__icon')
				.removeClass('hint__icon_stat-active');
		} else {
			ballonsWrap.html('');

			hintBox
				.find('.hint__icon')
				.removeClass('hint__icon_stat-active');
		}
	};

	var _closeHint = function() {
		var
			$this = $( this ),
			boxId = $this.data('hint-id');

		_removeBox( boxId );
	};

	return {
		init : init
	};
})();


$(function() {
	hints.init();
});