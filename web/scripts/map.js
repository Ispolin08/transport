var
	map,
	blurMap;

function initMap() {
	map = new google.maps.Map(document.getElementById('map'), {
		center           : {lat: 56.070963, lng: 92.902741},
		zoom             : 10,
		disableDefaultUI : true
	});

	setPorperWidth();

	blurMap = new google.maps.Map(document.getElementById('blurMap'), {
		center           : {lat: 56.070963, lng: 92.902741},
		zoom             : 10,
		disableDefaultUI : true,
		draggable: false,
		zoomControl: false,
		scrollwheel: false,
		disableDoubleClickZoom: true
	});

	google.maps.event.addListener(map, 'center_changed', function() {
		blurMap.setCenter( map.getCenter() );
	});

	google.maps.event.addListener(map, 'zoom_changed', function() {
		blurMap.setZoom( map.getZoom() );
	});
}

function setPorperWidth() {
	document.getElementById('blurMap').style.width = document.getElementById('map').offsetWidth + 'px';
}

window.addEventListener("resize", setPorperWidth);
