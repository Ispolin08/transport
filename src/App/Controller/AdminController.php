<?php
namespace App\Controller;

use App\Form\CalcType;
use App\Form\PageType;
use App\Service\BaseManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class AdminController extends Controller
{
    const ROUTE_TYPE_AIR = 0;

    const ROUTE_VVL = 1;
    const ROUTE_MVL = 0;

    const UNIT_ORDER = 0;
    const UNIT_M3 = 1;
    const UNIT_POSITION = 2;
    const UNIT_KG = 3;

    const ACT_MUL = 1;
    const ACT_PLUS = 2;


    /**
     * Конвертация базы по воздушным перевозкам
     * @Route("/admin/extract-air")
     */
    public function extractAirAction(Request $request)
    {
        $db = $this->get('database_connection');

        $db->query('Truncate table cargo');
        $db->query('Truncate table city');
        $db->query('Truncate table route');
        $db->query('Truncate table b');

        $cityMgr = $this->get('city_manager');
        $routeMgr = $this->get('route_manager');

        // LINKS
        $links = file_get_contents(__DIR__ . '/../../../web/uploads/links.csv');
        $lines = explode("\n", $links);

        unset($lines[0]);
        $modifiers = [];
        foreach ($lines as $line) {
            $items = explode(";", $line);
//                var_dump($items);
            if (strlen($items[0]) == 0) {
                break;
            }

            $vvl = explode(',', $items[2]);
            $mvl = explode(',', $items[3]);


            foreach ($vvl as $k => $v) {
                if (!empty($v)) {
                    $modifiers[] = trim($v);
                    $vvl[$k] = trim($v);
                }
            }
            foreach ($mvl as $k => $v) {
                if (!empty($v)) {
                    $modifiers[] = trim($v);
                    $mvl[$k] = trim($v);
                }
            }

            $cargo = [
                'name' => trim($items[0]),
                'code' => trim($items[1]),
                'vvl' => implode(',', $vvl),
                'mvl' => implode(',', $mvl)
            ];
            $db->insert('cargo', $cargo);
        }


        $modifiers = array_unique($modifiers);
        var_dump($modifiers);

        // BASE
        $base = file_get_contents(__DIR__ . '/../../../web/uploads/base.csv');
        $lines = explode("\n", $base);

        // line 0 validation
        $baseColsTmp = explode(';', $lines[0]); // шапка
        $baseCols = [];

        foreach ($baseColsTmp as $key => $bc) {
            if (mb_strpos($bc, 'заказ/кг') !== false) {
                $bc = str_replace('заказ/кг', '', $bc);
            }
            if (mb_strpos($bc, 'заказ') !== false) {
                $bc = str_replace('заказ', '', $bc);
                $baseColsUnit[$key] = self::UNIT_ORDER;
            }

            if (mb_strpos($bc, 'кубометр') !== false) {
                $bc = str_replace('кубометр', '', $bc);
                $baseColsUnit[$key] = self::UNIT_M3;
            }

            if (mb_strpos($bc, 'место') !== false) {
                $bc = str_replace('место', '', $bc);
                $baseColsUnit[$key] = self::UNIT_POSITION;
            }

            if (mb_strpos($bc, 'kg') !== false) {
                $bc = str_replace('kg', '', $bc);
                $baseColsUnit[$key] = self::UNIT_KG;
            }
            $baseCols[$key] = trim($bc);


        }


        var_dump('BASECOLS', $baseCols);

        foreach ($modifiers as $mod) {

            if (array_search($mod, $baseCols) == false) {
                var_dump('NOT FOUND ' . $mod);
            }
        }

        // Создаем столбцы как опции

        unset($lines[0]);
        foreach ($lines as $key => $line) {
            $cols = explode(';', $line);

            if (count($cols) < 3) {
                break;
            }

            // находим создаем оба города
            $cityFrom = $cityMgr->getOrCreateCityId($cols[0], true);
            $cityTo = $cityMgr->getOrCreateCityId($cols[1], true);

            echo $cols[0] . ' ' . $cityFrom . ' - ' . $cols[1] . ' ' . $cityTo . '<br>';
            ($cols[2] == 'MVL') ? $rtAirType = self::ROUTE_MVL : $rtAirType = self::ROUTE_VVL;


            //
//                col3 cur

            $rtId = $routeMgr->getOrCreateRouteId(
                [
                    'fromId' => $cityFrom,
                    'toId' => $cityTo,
                    'routeType' => self::ROUTE_TYPE_AIR,
                ],
                true
            );

            var_dump(
                [
                    'fromId' => $cityFrom,
                    'toId' => $cityTo,
                    'routeType' => self::ROUTE_TYPE_AIR,
                    'airType' => $rtAirType
                ]
            );
            foreach ($cols as $key => $col) {
                if ($key <= 1) {
                    continue;
                }

                if (empty($col)) {
                    continue;
                }

                var_dump($baseCols[$key] . ' - ' . $col);


                $price = str_replace(',', '.', $col);

                $act = null;

                if (mb_strpos($price, '*') !== false) {
                    $price = str_replace('*', '', $price);
                    $act = self::ACT_MUL;
                }
                if (mb_strpos($price, '+') !== false) {
                    $price = str_replace('+', '', $price);
                    $act = self::ACT_PLUS;
                }


                $b = [
                    'route' => $rtId,
                    'price' => $price,
                    'name' => $baseCols[$key],
                    'unit' => isset($baseColsUnit[$key]) ? $baseColsUnit[$key] : null,
                    'act' => $act
                ];

                $db->insert('b', $b);

            }
        }

        die('ok');
//        return $index;


    }

    /**
     * Конвертация базы по воздушным перевозкам
     * @Route("/admin/extract-water")
     */
    public function extractWaterAction(Request $request)
    {
        $db = $this->get('database_connection');
        $base = file_get_contents(__DIR__ . '/../../../web/uploads/water.csv');

        $cityMgr = $this->get('city_manager');
        $routeMgr = $this->get('route_manager');
        $db->query('Truncate table water_base');


        $lines = explode("\r\n", $base);
        unset($lines[0]);

        foreach ($lines as $line) {

            $items = explode(";", $line);
//                var_dump($items);
            if (strlen($items[0]) == 0) {
                break;
            }


            $from = $cityMgr->getOrCreateCityId($items[0], true);
            $to = $cityMgr->getOrCreateCityId($items[1], true);

            $b = [
                'from_id' => $from,
                'to_id' => $to,
                'min' => $items[2],
                'tarif' => $items[3]
            ];


            $db->insert('water_base', $b);
        }

        die('ok');
    }

    /**
     * Конвертация базы по воздушным перевозкам
     * @Route("/admin/extract-auto")
     */
    public function extractAutoAction(Request $request)
    {
        $db = $this->get('database_connection');
        $base = file_get_contents(__DIR__ . '/../../../web/uploads/auto.csv');
        $cityMgr = $this->get('city_manager');

        $db->query('Truncate table auto_base');

        $lines = explode("\n", $base);

        unset($lines[0]);

        foreach ($lines as $line) {

            $items = explode(";", $line);
//                var_dump($items);
            if (strlen($items[0]) == 0) {
                break;
            }


            $from = $cityMgr->getOrCreateCityId($items[0], true);
            $to = $cityMgr->getOrCreateCityId($items[1], true);

            $b = [
                'from_id' => $from,
                'to_id' => $to,
                'min' => $items[2],
                'm100' => $items[3],
                'm200' => $items[4],
                'm300' => $items[5],
                'm400' => $items[6],
                'm500' => $items[7],
                'm700' => $items[8],
                'm1000' => $items[9],
                'm1500' => $items[10],
                'm2000' => $items[11],
                'm2500' => $items[12],
                'm3000' => $items[13],
                'm3000plus' => $items[14]
            ];


            $db->insert('auto_base', $b);
        }

        die('auto ok');
    }

    /**
     * @Route("/admin/pages", name="admin_page_index")
     */
    public function pageIndexAction()
    {
        $pages = $this->get('page_manager')->findAll();
        return $this->render(':admin:page-index.html.twig', ['pages'=>$pages]);
    }

    /**
     * @Route("/admin/pages/edit/{page}", name="admin_page_edit")
     */
    public function pageEditAction(Request $r, $page)
    {
        $page= $this->get('page_manager')->find($page);

        $db = $this->get('database_connection');

        $form = $this->createForm(PageType::class, $page);
        if ($r->isMethod('POST'))
        {
            $form->handleRequest($r);
            if ($form->isValid())
            {
                $d = $form->getData();
                $sql = "UPDATE page SET title = :title, slug = :slug, body = :body WHERE id = :id";
                $stmt = $db->prepare($sql);
                $stmt->bindParam(':title', $d['title'], \PDO::PARAM_STR);
                $stmt->bindParam(':slug', $d['slug'], \PDO::PARAM_STR);
                $stmt->bindParam(':body', $d['body'], \PDO::PARAM_STR);
                $stmt->bindParam(':id', $page['id'], \PDO::PARAM_INT);
                $stmt->execute();

                return $this->redirectToRoute('admin_page_index');            
            }
        }

        return $this->render(':admin:page-edit.html.twig', ['page'=>$page, 'form'=>$form->createView()]);
    }


    /**
     * Конвертация базы по воздушным перевозкам
     * @Route("/admin")
     */
    public function indexAction()
    {
        return $this->render(':admin:index.html.twig', []);
    }
}