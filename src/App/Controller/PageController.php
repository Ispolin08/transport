<?php
namespace App\Controller;

use App\Form\CalcType;
use App\Service\BaseManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class PageController extends Controller
{
    /**
     * @Route("/{slug}.html", name="static_page")
     */
    public function viewAction(Request $request, $slug)
    {

        $page = $this->get('page_manager')->findPageBySlug($slug);
        return $this->render(':page:view.html.twig', ['page'=>$page]);

    }

}
