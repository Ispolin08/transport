<?php
namespace App\Controller;

use App\Form\CalcType;
use App\Service\BaseManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name = "homepage")
     */
    public function indexAction(Request $request)
    {

        // Варианты маршрута ИЗ
        $cityManager = $this->get('city_manager');
        $cargoManager = $this->get('cargo_manager');
        $from = $cityManager->getFrom();

        // Грузы
        $cargos = $cargoManager->getAllCargos();

        $form = $this->createForm(CalcType::class, [], ['from'=>$from, 'cargos'=>$cargos]);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $data =  $form->getData();
            $this->calculate($data);

            var_dump($data);
        }
//            else
//            {
//                var_dump($form->getErrors());
//                die('ERRR');
//            }



        return $this->render(':default:index.html.twig', ['form'=>$form->createView(), 'cargos'=>$cargos]);

    }

    private function calculate($formData)
    {
        $routeManager = $this->get('route_manager');
        $baseManager = $this->get('base_manager');
        $db = $this->get('database_connection');

        $totalDEBUG =0 ;
        var_dump($formData);


        // Берем данные о маршруте из базы
        $rt = $routeManager->getRouteId(
            ['fromId' => $formData['from'], 'toId' => $formData['to'], 'routeType' => 0 /*air*/]
        );

        $totalOrder = [];
        foreach ($formData['positions'] as $pos)
        {

            $totalPrice = [];

            echo "Расчет позиции заказа: <br>";
            var_dump($pos);

            $weight = str_replace(',','.', $pos['weight']);
            $V = (float)str_replace(',','.', $pos['vol']);
            $place = (int)$pos['place'];

            // считаем объем и сверяем с весом
            $maxV = $V * 167;
            var_dump('Объем груза '.$V .' * 167='. $maxV);



            if ($maxV > $weight) {
                var_dump('Вес по объему больше нашего веса. Берем '.$maxV);
                $maxV = $this->roundHalf($maxV);

                $weight = $maxV;
                // Проверка на мин

                $t = $baseManager->getTarifForWeight($weight, $rt);
            } else
            {
                $min = $baseManager->getPriceParamByRouteId('Min', $rt);
                $norm = $baseManager->getPriceParamByRouteId('norm', $rt);

                var_dump('сравниваем '.$weight.' с МИН весом', $min);


                // Проверка на мин
                if ($min > $weight) {
                    $weight = $min;
                    $t = $norm/$min;
                    echo "Берем мин вес и норм тариф";
                }
                else{
                    echo "Отсавляем наш вес ".$weight."<br>";
                    $t = $baseManager->getTarifForWeight($weight, $rt);
                }

            }

            echo "<h3>Пластный вес " . $weight .  " Тариф " . $t.  "</h3><br>";




            $routeVVL = $baseManager->getPriceParamByRouteId('Lines', $rt);

            // набор параметров из линков
            $sql = "SELECT * FROM cargo WHERE id = ?";
            $cargo = $db->fetchAssoc($sql, [$formData['cargoType']]);

            var_dump($cargo);
            if ($routeVVL == 'VVL') {
                $params = $cargo['vvl'];
            } else {
                $params = $cargo['mvl'];
            }

            $codeCell = $baseManager->getValueParamByRouteId($cargo['code'], $rt);

            if (!$codeCell)
            {
                var_dump('Не найден или пуст код !'.$cargo['code']);
                $codePrice = $t;

            } else {
                echo "<h3>";
                if ($codeCell['act'] == BaseManager::ACT_MUL) {
                    $codePrice = $t * $codeCell['price'];
                    echo "Код :" . $cargo['code'] . '* ' . $codeCell['price'] . ' = ' . $codePrice . "<br>";
                }

                if ($codeCell['act'] == BaseManager::ACT_PLUS) {
                    $codePrice = $t + $codeCell['price'];
                    echo "Код :" . $cargo['code'] . ' + ' . $codeCell['price'] . ' = ' . $codePrice . "<br>";
                }

                $codePrice = $weight * $codePrice;
                echo "Вес на тариф = ".$codePrice."<br>";
            }


            $totalPrice[]=$codePrice;

            echo "</h3>";
            echo "Параметры для этого груза " .$params ."<br>";

            $params = explode(',',$params);
            foreach ($params as $param)
            {
                $paramCell = $baseManager->getValueParamByRouteId($param, $rt);

                if ($paramCell['name']=='Tax 999')
                {
                    echo "<h3>расчет TAX<br>";
                    $totalOrder[$paramCell['name']]= $this->calcTAX($weight, $rt);
                    echo "</h3>";
                    continue;
                }
                $mult=0;
                if ($paramCell['unit']== BaseManager::UNIT_KG)
                {
                    echo "На кг <br>";
                    $mult = $weight;

                }
                if ($paramCell['unit']== BaseManager::UNIT_ORDER)
                {
                    echo "на заказ<br>";
                    $mult = 1;
                }
                if ($paramCell['unit']== BaseManager::UNIT_POSITION)
                {
                    echo "на место<br>";
                    $mult = $place;
                }
                $res =  $mult * $paramCell['price'];

                echo "<h3>".$paramCell['name'].' '.$paramCell['price'].'*'.$mult.' = '.$res.'</h3><br>';


                if ($paramCell['unit']== BaseManager::UNIT_ORDER)
                {
                    if (!isset($totalOrder[$paramCell['name']])) {
                        $totalOrder[$paramCell['name']] = $res;
                    }
                }else
                {
                    $totalPrice[] = $res;
                }
            }


            $sum = 0;
            foreach ($totalPrice as $tp) {
                $sum += $tp;
            }
            var_dump('Итого по позиции' . $sum);
            $totalDEBUG+=$sum;

            echo "<hr>";
        }

        $sum = 0;
        foreach ($totalOrder as $to) {
            $sum += $to;
        }

        echo "Еще добавляем расчет на весь заказ ".$sum."<br>";
        $totalDEBUG+=$sum;

        echo "ИТОГО ".$totalDEBUG;
        die();

//        $AWB = $dS->getPriceParamByRouteId('AWB', $rt);
//        echo "AWB Плюсуеем  " .$AWB . "<br>";



    }


    public function roundHalf($param)
    {

        if ($param - floor($param) > 0.5) {
            return ceil($param);
        }
        if ($param - floor($param) ==0 ) {
            return $param;
        }
        return floor($param) + 0.5;

    }

    public function calcTAX($w,  $rt)
    {
        $baseM = $this->get('base_manager');
        if ($w<999) {
            $TAX = $baseM->getPriceParamByRouteId('Tax 999', $rt);
            echo "Меньше 999  +" .$TAX ." <br>";
            return $TAX;
        }
    }

}