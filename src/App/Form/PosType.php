<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints;


class PosType extends AbstractType
{
    public function getName()
    {
        return 'postype';
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('place', TextType::class, ['label' => 'Мест'])
            ->add('weight', TextType::class, ['label' => 'Вес'])
            ->add('vol', TextType::class, ['label' => 'Объем']);


    }

    public function getExtendedType()
    {
        return 'form';
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'csrf_protection' => false,
            )
        );
    }


}


