<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Choice;

class CalcType extends AbstractType
{

    public function getName()
    {
        return 'calctype';
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

//        var_dump($options);
//        die();
        $builder
            ->add('from', ChoiceType::class, ['choices' => $options['from']])
            ->add('to', ChoiceType::class, ['choices' => $options['from']])
            ->add('cargoType', ChoiceType::class, ['choices' => $options['cargos']])
            ->add(
                'positions',
                CollectionType::class,
                array('entry_type' => PosType::class, 'allow_add' => true, 'label' => false,

                )
            );

    }

    public function getExtendedType()
    {
        return 'form';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'validation_groups' => array('registration'),
            'from' => [],
            'cargos' => [],
            'data_class' => null,
            'csrf_protection' => false,

        ));
    }


}


