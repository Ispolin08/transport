<?php
namespace App\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class MenuBuilder
{
    private $factory;

    /**
     * @param FactoryInterface $factory
     *
     * Add any other dependency you need
     */
    public function __construct(FactoryInterface $factory)
    {
        $this->factory = $factory;
    }

    public function createMainMenu(array $options)
    {
        $menu = $this->factory->createItem('root');

        $menu->addChild('Кальк-р.', array('route' => 'homepage'));
        // ... add more children


        $menu->addChild('Услуги-р.', array('route' => 'static_page', 'routeParameters'=>['slug'=>'services']));
        $menu->addChild('Контакты', array('route' => 'static_page', 'routeParameters'=>['slug'=>'contacts']));



        return $menu;
    }
    public function createAdminMenu(array $options)
    {
        $menu = $this->factory->createItem('root');

        $menu->addChild('Стат страницы', array('route' => 'admin_page_index'));



        return $menu;
    }
}