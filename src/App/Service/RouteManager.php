<?php
namespace App\Service;

class RouteManager
{

    private $db;

    /**
     * CityManager constructor.
     * @param $db
     */
    public function __construct($db)
    {
        $this->db = $db->get('database_connection');
    }


    public function getRouteId($params, $create = false)
    {
        $sql = "SELECT * FROM route WHERE fromId = :fromId and toId = :toId and routeType = :routeType ";
        return  $this->db->fetchAssoc($sql, $params)['id'];
    }


    public function getOrCreateRouteId($params, $create = false)
    {
        $sql = "SELECT * FROM route WHERE fromId = :fromId and toId = :toId and routeType = :routeType ";
        $route = $this->db->fetchAssoc($sql, $params);

        if (!$route) {
            if ($create) {
                $this->db->insert('route', $params);

                return $this->getOrCreateRouteId($params, false);
            } else {
                return false;
            }
        }

        return (int)$route['id'];
    }

}