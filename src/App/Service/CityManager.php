<?php
namespace App\Service;

class CityManager
{

    private $db;

    /**
     * CityManager constructor.
     * @param $db
     */
    public function __construct($db)
    {
        $this->db = $db->get('database_connection');
    }


    public function getFrom()
    {
        $sql = "SELECT * FROM city ";


        $b = $this->db->fetchAll($sql, []);

        $res = [];
        foreach ($b as $c) {
            $res[$c['name']] = $c['id'];
        }

        return $res;
    }

    public function getOrCreateCityId($name, $create = false)
    {
        $sql = "SELECT * FROM city WHERE name = ?";
        $city = $this->db->fetchAssoc($sql, array($name));


        if (!$city)
        {
            if ($create) {
                $this->db->insert('city', ['name' => $name]);
                return $this->getOrCreateCityId($name, false);
            } else {
                return false;
            }
        }
        return (int)$city['id'];
    }

    

}