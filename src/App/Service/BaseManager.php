<?php
namespace App\Service;

class BaseManager
{

    const ACT_MUL = 1;
    const ACT_PLUS = 2;

    const UNIT_ORDER = 0;
    const UNIT_M3 = 1;
    const UNIT_POSITION = 2;
    const UNIT_KG = 3;

    private $db;

    /**
     * CityManager constructor.
     * @param $db
     */
    public function __construct($db)
    {
        $this->db = $db->get('database_connection');
    }


    public function getPriceParamByRouteId($paramName, $routeId)
    {
        $sql = "SELECT * FROM b WHERE name = :name and route = :rt";
        $b = $this->db->fetchAssoc($sql, ['name' => $paramName, 'rt' => $routeId]);

        return $b['price'];

    }

    public function getValueParamByRouteId($paramName, $routeId)
    {
        $sql = "SELECT * FROM b WHERE name = :name and route = :rt";
        $b = $this->db->fetchAssoc($sql, ['name' => $paramName, 'rt' => $routeId]);

        return $b;

    }

    public function getTarifForWeight($weight, $rt)
    {
        $wLine = [45, 100, 250, 500, 1000];

        foreach ($wLine as $key=>$wL) {
            if ($wL > (float)$weight)
            {
                $wIdx = $key-1;
                break;
            }
        }

        if (!isset($wIdx)) {
            $wIdx = $key;
        }

        $t = $this->getPriceParamByRouteId((string)$wLine[$wIdx], $rt);

        return $t;
    }


}