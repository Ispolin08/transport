<?php
namespace App\Service;

class CargoManager
{

    private $db;

    /**
     * CityManager constructor.
     * @param $db
     */
    public function __construct($db)
    {
        $this->db = $db->get('database_connection');
    }


    public function getAllCargos()
    {
        $sql = "SELECT * FROM cargo ";
        $b = $this->db->fetchAll($sql, []);

        $res = [];
        foreach ($b as $c)
        {
            $res[$c['name']]=$c['id'];
        }

        return $res;
    }


}