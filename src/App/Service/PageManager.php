<?php
namespace App\Service;

class PageManager
{

    private $db;

    /**
     * CityManager constructor.
     * @param $db
     */
    public function __construct($db)
    {
        $this->db = $db->get('database_connection');
    }


    public function findPageBySlug($slug)
    {
        $sql = "SELECT * FROM page WHERE slug = :slug ";
        $b = $this->db->fetchAssoc($sql, ['slug' => $slug]);

        return $b;

    }

    public function find($id)
    {
        $sql = "SELECT * FROM page WHERE id = :id ";
        $b = $this->db->fetchAssoc($sql, ['id' => $id]);

        return $b;

    }


    public function findAll()
    {
        $sql = "SELECT * FROM page";
        $b = $this->db->fetchAll($sql);
        return $b;

    }

}